import React from "react";
import Button from "./common";

import "./css/configuration.css"


class CommandField extends React.Component {
  render() {
    return (
      <div className="field">
        <label>
          Alias
          <input
            type="text"
            data-address={`${this.props.index},0`}
            name={`alias-${this.props.index}`}
            defaultValue={this.props.alias}
            onChange={this.props.handleChange}
            placeholder="Weather"/>
        </label>
        <label>
          Phrase
          <input
            type="text"
            data-address={`${this.props.index},1`}
            name={`utterance-${this.props.index}`}
            defaultValue={this.props.utterance}
            onChange={this.props.handleChange}
            placeholder="What's the weather"/>
        </label>
        <div className="minus">
          <Button
            className="remove"
            data-index={this.props.index}
            handleClick={this.props.handleRemove}
            value="➖"/>
        </div>
      </div>
    );
  }
}


export default class Configuration extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      hostname: props.hostname,
      commands: props.commands,
    }

  }

  getPermalink = () => {
    const base = `${window.location.protocol}//${window.location.host}/`
    let commands = ""
    if (this.state.commands.length) {
      commands = "&commands=" + encodeURI(JSON.stringify(this.state.commands));
    }
    return `${base}?hostname=${this.state.hostname}${commands}`;
  }

  handleSubmit = (event) => {

    event.preventDefault();

    this.props.handleConfigurationUpdate(
      this.state.hostname,
      this.state.commands.filter((command) => {
        return (command[0] && command[1]);
      }),
    );

  }

  handleChange = (event) => {
    if (event.target.name === "hostname") {
      this.setState({hostname: event.target.value});
    } else {
      const [x, y] = event.target.getAttribute("data-address").split(/,/);
      let commands = this.state.commands.slice();
      commands[x][y] = event.target.value;
      this.setState({commands: commands});
    }
  }

  handleRemove = (event) => {
    this.setState(
      {
        commands: this.state.commands.splice(
          ~~event.target.getAttribute("data-index"), 1
        )
      }
    );
  }

  addCommandField = (event) => {
    event.preventDefault();
    this.setState({"commands": this.state.commands.concat([["", ""]])});
  }

  render() {

    const commands = this.state.commands.map(
      (command, index) =>
        <CommandField
          key={index}
          index={index}
          alias={command[0]}
          utterance={command[1]}
          handleChange={this.handleChange}
          handleRemove={this.handleRemove}
        />
    );

    return (
      <div className="configuration">
        <h2>Configuration</h2>
        <form onSubmit={this.handleSubmit}>

          <h3>Basics</h3>

          <div className="field">
            <label>
              Mycroft Hostname
              <input
                type="text"
                name="hostname"
                defaultValue={this.props.hostname}
                onChange={this.handleChange}
                placeholder="localhost"/>
            </label>
            <div className="help">
              This will be the hostname (or IP) of the machine that's running
              Mycroft. If you're running this on your the same device, just
              use <code>localhost</code>.
            </div>
          </div>

          <h3>Commands</h3>
          <div className="commands">
            {commands}
          </div>
          <div className="button-row">
            <Button
              className="command-append"
              handleClick={this.addCommandField}
              value="➕"/>
          </div>

          <div className="field">
            <input type="submit" name="s" value="Save"/>
          </div>

          <h3>Permalink</h3>
          <p>
            You can automatically configure this page with the above values by
            going to this URL:
          </p>
          <pre className="permalink">
            {this.getPermalink()}
          </pre>

        </form>
      </div>
    );
  }
}
