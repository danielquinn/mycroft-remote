import React from "react";
import ReactDOM from "react-dom";

import Configuration from "./configuration";
import Main from "./main"

import "./css/index.css";
import "./css/forms.css"
import "./css/swipe-view.css"


class App extends React.Component {

  constructor(props) {

    super(props);

    this.LISTEN = "listen"
    this.STOP = "stop"

    const urlParams = new URLSearchParams(window.location.search);
    const urlHostname = urlParams.get("hostname");
    const urlCommands = urlParams.get("commands");
    this.socket = null;


    this.state = {
      hud: "Test This",
      isConnected: false,
      hostname: urlHostname || "",
      commands: (urlCommands) ? JSON.parse(urlCommands) : [],  // [[alias, utterance],...]
    }

    if (this.state.hostname) {
      this.connect();
    }

  }

  connect = () => {

    const LISTEN_STARTED = "recognizer_loop:record_begin"
    const LISTEN_STOPPED = "recognizer_loop:record_end"
    const SPEAK = "speak"

    if (this.state.isConnected) {
      this.socket.close();
    }

    if (!this.state.hostname) {
      console.warn("No hostname available.  Connection aborted.");
      this.setState({isConnected: false});
      return;
    }

    this.socket = new WebSocket(`ws://${this.state.hostname}:8181/core`);

    this.socket.onopen = () => {
      console.log("Connection to Mycroft established");
      this.setState({isConnected: true});
    };

    this.socket.onerror = (error) => {
      console.error(error);
    };

    this.socket.onclose = (event) => {
      if (event.wasClean) {
        console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
      } else {
        console.error("[close] Connection died");
      }
    };

    this.socket.onmessage = (event) => {

      if (event.type !== "message") {
        return
      }

      let payload = JSON.parse(event.data);

      switch (payload.type) {

        case LISTEN_STARTED:
          this.setState({hud: "[ Listening ]"});
          break;

        case LISTEN_STOPPED:
          this.setState({hud: ""});
          break;

        case SPEAK:
          this.setState({hud: payload.data.utterance});
          setTimeout(
            () => {
              this.setState({hud: ""})
            },
            5000
          );
          break;

        default:
          break;

      }

    };

  }

  handleConfigurationUpdate = (hostname, commands) => {
    this.setState(
      {
        hostname: hostname,
        commands: commands,
      },
      this.connect
    );
  }

  handleButtonClick = (event, message) => {

    switch (message) {

      case this.LISTEN:
        this.setState({hud: "[ Listening ]"});
        this.socket.send('{"type": "mycroft.mic.listen", "data": {}}');
        break;

      case this.STOP:
        this.setState({hud: ""});
        break;

      default:
        this.socket.send(
          JSON.stringify(
            {
              "type": "recognizer_loop:utterance",
              "data": {
                "utterances": [message],
                "lang": "en",
                "session": "session_id"
              }
            }
          )
        );
        break;
    }

  }

  render() {
    return (
      <div className="swipe-view">

        <section>
          <Main
            LISTEN={this.LISTEN}
            STOP={this.STOP}
            commands={this.state.commands}
            hud={this.state.hud}
            isConnected={this.state.isConnected}
            handleButtonClick={this.handleButtonClick}/>
        </section>

        <section>
          <Configuration
            hostname={this.state.hostname}
            commands={this.state.commands}
            handleConfigurationUpdate={this.handleConfigurationUpdate}/>
        </section>

      </div>
    );
  }

}


ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
