import React from "react";
import Button from "./common";

import "./css/main.css";
import "./css/rotation.css";


export default class Main extends React.Component {

  showInterface = () => {

    if (!this.props.isConnected) {

      return (
        <div className="configuration-notice">
          « Swipe to configure
        </div>
      );

    } else {

      const commands = this.props.commands.map(
        (command, index) =>
          <div className="slot">
            <Button
              key={index}
              className="command"
              value={command[0]}
              handleClick={(event) => this.props.handleButtonClick(event, command[1])}/>
          </div>
      );

      return (
        <div className="interface">
          <div className="slot">
            <Button
              className="listen"
              value="Listen"
              handleClick={(event) => this.props.handleButtonClick(event, this.props.LISTEN)}/>
          </div>
          <div className="slot">
            <Button
              className="stop"
              value="Stop"
              handleClick={(event) => this.props.handleButtonClick(event, this.props.STOP)}/>
          </div>
          {commands}
        </div>
      );
    }
  }

  render() {
    return (
      <div className="main">
        <h2>Mycroft Remote</h2>
        {this.showInterface()}
        <div className="hud">
          {this.props.hud}
        </div>
        <img
          src="/logo512.png"
          className="logo rotate linear infinite"
          alt="Logo"/>
      </div>
    );
  }
}
